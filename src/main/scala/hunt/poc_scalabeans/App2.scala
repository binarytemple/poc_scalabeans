package hunt.poc_scalabeans

import org.scalastuff.scalabeans.Preamble._
import org.scalastuff.scalabeans.PropertyDescriptor
import org.scalastuff.scalabeans.types._
import java.util.Date

case class Person2 (
  var human:Option[Boolean] = None,
  var firstName:Option[String] = None,
  var lastName:Option[String] = Some("last"),
  var dateOfBirth:Option[Date] = Some(new Date(0))
)
                
object Runner2 extends App {

  import org.scalastuff.scalabeans.Preamble._
  import org.scalastuff.scalabeans.PropertyDescriptor
  import org.scalastuff.scalabeans.types.OptionType
  import org.scalastuff.scalabeans.types.StringType
  import java.util.Date

  def bean2CreateTableSQL[T <: AnyRef](implicit m: scala.reflect.Manifest[T]) = {
    val IDField = """(.*)_(id)""".r    
    val descriptor = descriptorOf[T](m)
    for (property <- descriptor.properties) {
      println(property.name + ": " + property.scalaType)
      property match {
        case a: PropertyDescriptor => a.scalaType match {
          case OptionType(StringType) => println("String type detected")
          case OptionType(IntType) => println("Int type detected")
          case OptionType(BooleanType) => println("Boolean type detected")
          case OptionType(DateType) => { println("Date type detected")}
          case unk => { println("Unknown type detected: %s".format(unk))}
        }
      }
    }
  }
  bean2CreateTableSQL[Person2]
}
