package hunt.poc_scalabeans

import org.scalastuff.scalabeans.Preamble._
import org.scalastuff.scalabeans.PropertyDescriptor
import org.scalastuff.scalabeans.types.OptionType
import org.scalastuff.scalabeans.types.StringType
import java.util.Date

case class Person (
  var firstName:Option[String] = None,
  var lastName:Option[String] = Some("last"),
  var dateOfBirth:Option[Date] = Some(new Date(0))
)
                

object Runner extends App {
  //http://code.google.com/p/scalabeans/wiki/GettingStarted
  val descriptor = descriptorOf[Person]
  for (property <- descriptor.properties)
    println(property.name + ": " + property.scalaType)
    
   //http://code.google.com/p/scalabeans/wiki/BeanInstances#Property_values
   val p = new Person
   println(p)
   descriptor.set(p, "firstName", Some("dave"))
   println(p)
   
   val m = Map("firstName" -> "steve","lastName" -> "jones")
   
   for(k <- m.keys) {
     descriptor.property(k).getOrElse(None) match {
     case a:PropertyDescriptor => a.scalaType  match {
       case OptionType(StringType) => descriptor.set(p, k, m.get(k)) 
     }
     case b => println("unknown")
     } 
   }
   println(p)
}